# OpenML dataset: 7k-Books

https://www.openml.org/d/43542

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Do we really need another dataset of books?
My initial plan was to build a toy example for a recommender system article I was writing. After a bit of googling, I found a few datasets. Sadly, most of them had some issues that made them unusable for me (e.g, missing description of the book, a mix of different languages but no column to specify the language per row or weird delimiters). 
So I decided to make a dataset that would match my purposes.
First, I got ISBNs from Soumik's Goodreads-books dataset. Using those identifiers, I crawled the Google Books API to extract the books' information.
Then, I merged those results with some of the original columns from the dataset and after some cleaning I got the dataset you see here.
What can I do with this?
Different Exploratory Data Analysis, clustering of books by topics/category, content-based recommendation engine using different fields from the book's description. 
Why is this dataset smaller than Soumik's Goodreads-books?
Many of the ISBNs of that dataset did not return valid results from the Google Books API. I plan to update this in the future, using more fields (e.g., title, author) in the API requests, as to have a bigger dataset.
What did you use to build this dataset?
Check out the repoistory here Google Books Crawler
Acknowledgements
This dataset relied heavily on Soumik's Goodreads-books dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43542) of an [OpenML dataset](https://www.openml.org/d/43542). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43542/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43542/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43542/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

